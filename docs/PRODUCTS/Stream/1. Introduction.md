---
tags: [PRODUCTS]
---

# Introduction

Stream APIs let you access the income and employment information of your users.

### Income API

Income API contains information about your user's income from either connected work platform or uploaded pay stub. Only those information are provided for which your user has given consent during connect time.

### Employment API

Employment API contains information about your user's employment information from either connected work platform or uploaded pay stub. Only those information are provided for which your user has given consent during connect time.
