---
tags: [PRODUCTS]
---

# Implementation Guide

Welcome to Phyllo! Phyllo makes it easy for your customers to connect their payroll account to your application or upload pay stubs.

## Getting Started with Phyllo

At this point, you should be set up with access to our Developer Dashboard. If you don’t have access yet, please contact us at contact@getphyllo.com.

In the Developer Dashboard you will find your API Key and API Secret.

### API Keys & Modes

Under the API Key tab you will see your API Key and API Secret. You will initially have access to our Sandbox mode, and can request access to Production from the Phyllo team via contact@getphyllo.com. Each mode will have its own API Secret.

    
### Test Console

Prior to diving in with development work, we recommend familiarizing yourself with SDK in our Test Console. The Test Console will allow you to create SDK tokens and launch Phyllo SDK. You can see how different SDK token configurations affect the front end UI, and test different front-end workflows. In Sandbox, you have to use our test credentials which can be found here. Live credentials will not work in Sandbox. In order to test with real payroll credentials you will need to use the Development mode.

### Employer List

The Employers list in our dashboard will make it easy for you to navigate our coverage and confirm that the employers you care about most are supported by Phyllo.

## Phyllo SDK

Phyllo SDK allows users to seamlessly authenticate with their payroll platform login credentials, and authenticate access to their payroll account. We highly recommend leveraging our SDKs (Web, iOS) for development.

### SDK tokens

To initialize SDK you will need to generate a short-lived SDK token. Your server side code will generate the SDK token by sending a request to our /sdktoken endpoint. SDK tokens are single-use and expire after 15 minutes. SDK tokens define your user experience and tell Phyllo what jobs you need us to complete. For product specific SDK token recommendations please read our product guides: Direct Deposit, Income and Employment.
Customization

We understand how important it is to have a cohesive user experience, so we’ve made Phyllo SDK highly customizable. All customizations are done via SDK token creation.

### Customization

Intro Screen: It is up to you to determine whether you want to leverage Phyllo’s intro screen. If you decide to manage your own intro screen, some things to consider are security, privacy, and context. It is very important for the end-user to understand why you are asking them to connect their Payroll account. In order to skip the Phyllo intro screen, pass through skip_intro_screen: true when creating your SDK token.

Search Screens: You have the option of using our search, or building your own.

Leverage the Phyllo Search screen. Phyllo’s user flow is optimized for conversion, and we see the highest conversion where customers know their payroll platform. As such, we will pre-populate SDK with the most popular Platforms and then allow the user to search for either their employer or platform by keyword. Using our search screen will be the fastest path to integration, and the easiest to maintain (we do it all for you!).

Building your own search screen. If you want to have full control over the search experience, we have several APIs that will allow you to build your own search screen. There are two paths for building your own search:

Build your search functionality on top of your own Database.

Retrieve our list of supported Platforms and Employers, and store the name in addition to the employer_id or platform_id in your database. You will want to update this information daily as our coverage is always expanding.

Filter out institutions that do not support your required jobs using the supported_jobs field.
Build customized search screens on top of the Phyllo Platform and Employer data stored in your database.

Log the selected employer_id or platform_id when the user selects a Phyllo provider and pass it through when creating your SDK token. This will notify us to skip our search screen. If you do not pass through an employer_id or platform_id our search screens will be enabled.

## Monitoring
### SDK Events

SDK events will give you visibility into what is happening as the user moves through Phyllo SDK. We have five different SDK event types. It is up to you to determine how you would like to use these events for tracking. Some ways we recommend using SDK events include:

Listen for the onError callbacks to troubleshoot SDK issues. This information will not be visible via the API once the user closes SDK. Highly recommended.

Listen for onEvent:input_amount to understand the dollar value of the direct deposit switch. This is also returned via the direct deposit webhooks.

Listen for the onLogin callback to know if a user successfully connected their payroll account and retrieve the account_id. This is also returned via the account.added webhook.

Listen for the onEvent:select_platform and onEvent:select_employer callbacks to understand what Platform or Employer the user connected to. The platform_id is also available via the Account endpoints.

Listen to the onSuccess callback to know that the job has successfully completed. This can also be consumed via our product webhooks.

### Webhooks and Jobs

Webhooks are a crucial component of any optimized Phyllo integration. In order to confirm that a direct_deposit job has completed, you will need to listen for the direct_deposit_switch.added or direct_deposit_payment.added webhooks. Instructions for subscribing to Webhooks (and some other nuances) can be found here.

For direct deposit jobs, everything you need to know will be included in the webhook. Most importantly, it will return the outcome field (failure or success). If for some reason you miss the webhook, you can query our Jobs endpoint. To filter for jobs relevant to a specific user, you can query by link_token_id or account_id.

### Data storage

Storing the correct Identifiers is a crucial part of optimizing your Phyllo integration. There are three key Phyllo Identifiers that you should store on your end: LinkToken.id, Account.id, and Job.id. You can see full descriptions for those Identifiers here.
