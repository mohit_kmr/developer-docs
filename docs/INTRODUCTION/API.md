---
tags: [INTRODUCTION]
---

### Introduction
The Phyllo API is built on RESTful principles. All request and response payloads are encoded in JSON. For security purposes, all requests must be sent through HTTPS. To get access to the Developer Dashboard and interface with the API, please contact contact@phyllo.com.



### Modes
The API is available in three modes that can be accessed by sending requests to different servers. Each API Key you are granted will be associated with a single mode.



Mode | Host | Description
---------|----------|---------
 Sandbox | https://api-sandbox.getphyllo.com | Use Sandbox mode to build and test your integration. In this mode, you must use test credentials to authenticate with payroll platforms. All API endpoints will return mock data and no actual updates are made to any payroll account.
 Development | https://api-development.getphyllo.com | Use Development mode can be used to test your integration before going live in Production. In this mode, you use real credentials to authenticate with payroll platforms. API endpoints return real data and updates are made to payroll accounts.
  Production | https://api.getphyllo.com | Use Production mode to go live with your integration. Your end-users will use their login credentials to authenticate with their payroll accounts. API endpoints return real data and updates are made to payroll accounts. Note that in this mode, all API calls are billable.


### Authentication
You can access the API Keys and Secrets for each mode in the Developer Dashboard.

Your API Key is a unique identifier used by Phyllo for logging and troubleshooting. Your API secret must be included in all requests to the API via a custom HTTP header. You will have a different API Secret for each mode.


Header | Description
---------|----------
 Authorization | Basic BASE64(API Key:API Secret)


