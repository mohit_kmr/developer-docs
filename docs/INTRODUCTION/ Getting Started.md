---
tags: [INTRODUCTION]
---

# Getting-Started
### Welcome

Welcome to Phyllo, the API for Payroll. Phyllo makes it easy for your customers to grant you access to their payroll account via your application, unlocking a wealth of data and functionality. Once user access is granted, your application can update direct deposit allocations and access income and employment data from their payroll account. To request access to the Phyllo API, please contact contact@getphyllo.com.

### Implementation Overview
The Phyllo API is easy to integrate and highly customizable. Implementing Phyllo involves a simple client and server side integration, which be can outlined in four key steps:

SDK token: Your application server sends a request to our REST API to generate a short-lived SDK token.

Phyllo Connect: Using the SDK token, your application client initializes the client SDK to launch the Phyllo SDK model. Your end-users interact with SDK to submit their login credentials to authenticate with their payroll provider over a secure and encrypted connection.

Account Creation: Once the user successfully authenticates via Phyllo SDK an account is created representing the user's payroll account.

Webhooks: Webhooks are delivered to your server in real time, notifying your application whenever data is available or updated. Your server can then fetch data using our REST API.


