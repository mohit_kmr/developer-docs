---
tags: [INTRODUCTION]
---

# Errors

### API Errors
Phyllo API error messages are also returned in JSON format inside an error object. The type field contains a higher level category of the error and the code field is more specific. The message field contains a human-readable description of the error and is subject to change. An example is:
```json
{
  "error": {
    "type": "INVALID_REQUEST_PARAMETERS",
    "code": "MISSING_FIELDS",
    "message": "Request is missing required fields: [enabled, name].",
    "status_code": 400
  }
}
```

There are four different error types:


Error Type | Description
---------|----------
 RECORD_NOT_FOUND | A requested record could not be found.
 INVALID_REQUEST_PARAMETERS | A request was invalid.
 UNAUTHORIZED_REQUEST | A request was unauthorized.
 UNKNOWN_ERROR | An unknown error occurred.



### SDK Errors
An onError event will fire for every error. Errors may be retryable (e.g. an incorrect credential was entered) or may be terminal (e.g. the 3rd party integration is currently down). If an error occurred before closing the modal, SDK's onExit callback is called with an error object as well.

SDK error codes and types are safe for developer use, whereas error messages are liable to change.

There are multiple different error types:


Error Type | Description
---------|----------
 networkError | The end-user's device is offline.

Please contact contact@getphyllo.com for access to our Developer Dashboard.